/**
 * Purpose: This script file draws the wall and floors
 * Created by: Reilly Webster
 * Subject: CIS 1246 Internet Programming Part 1
 * Language: PIXI JS and Javascript
 * Date: April 18, 2019
 */

/**
 * Draws shapes to be used as walls and demonstrate PIXI's JS features for drawing graphics
 */
function loadGraphics(){
    let graphics = new PIXI.Graphics();
    // Rectangle // Rectangle
    graphics.beginFill(0x1719B4);
    graphics.drawRect(app.screen.width/2, app.screen.height/2, 50, 50);
    graphics.endFill();
    // Rectangle // Rectangle
    graphics.beginFill(0x3DB417);
    graphics.drawRect(0, app.screen.height, app.screen.width, -32);
    graphics.endFill();
    // Circle
    graphics.lineStyle(2, 0xFFFFFF, 1); // draw a circle, set the lineStyle to zero so the circle doesn't have an outline
    graphics.beginFill(0xDE3249);
    graphics.drawCircle(100, 250, 50);
    graphics.endFill();
    // Adds all graphics to stage
    app.stage.addChild(graphics);
}
/**
 * This function displays a message to the user using PIXI's style and text rendering. This code was borrowed and altered
 * from PIXI JS examples.
 * Source: https://pixijs.io/examples/#/text/text.js
 */
function loadMessage(){
    var style = new PIXI.TextStyle({
        fontFamily: 'Arial',
        fontSize: 24,
        fontStyle: 'italic',
        fontWeight: 'bold',
        fill: ['#ffffff', '#00ff99'], // gradient
        stroke: '#CA24A9',
        strokeThickness: 5,
        dropShadow: true,
        dropShadowColor: '#000000',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 6,
        dropShadowDistance: 6,
        wordWrap: true,
        wordWrapWidth: 440
    });
    window.richText = new PIXI.Text('Reilly Webster - 2D Platformer' + "\nProgramming Language: PIXI JS"
                                + "\nDate: April 19, 2019" + "\nSubject: CIS 2286 Internet Programming Part 1", style);
    richText.x = app.screen.width * 0.5;
    richText.y = app.screen.height * 0.05;
    app.stage.addChild(richText);
}
/**
 * This function loads a test block to use for collisions
 */
function loadCollisionBlock(){
    window.collisionBlock = new Sprite.from("images/objects/testBlock.png");
    collisionBlock.x = app.screen.width/2;
    collisionBlock.y = app.screen.height - (collisionBlock.width*2);
    app.stage.addChild(collisionBlock);
}