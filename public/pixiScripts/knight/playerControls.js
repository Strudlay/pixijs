/**
 * Purpose: This script file checks whether keys are being pushed. This is a custom script acquired from kittykatattack's
 * tutorial. Source included below.
 * Source: https://github.com/kittykatattack/learningPixi#keyboard
 * Subject: CIS 1246 Internet Programming Part 1
 * Language: PIXI JS and Javascript
 * Edited by: Reilly Webster
 * Date: April 16, 2019
 */

/**
 * Accepts the key being pushed into the parameter. Then uses this key to check whether it is active or not.
 * @param value
 */
function keyboard(value) {
    // Initialize variables
    let key = {};
    key.value = value;
    key.isDown = false;
    key.isUp = true;
    key.press = undefined;
    key.release = undefined;
    // The `downHandler`
    key.downHandler = event => {
        if (event.key === key.value) {
            if (key.isUp && key.press) key.press();
            key.isDown = true;
            key.isUp = false;
            event.preventDefault();
        }
    };
    // The `upHandler`
    key.upHandler = event => {
        if (event.key === key.value) {
            if (key.isDown && key.release) key.release();
            key.isDown = false;
            key.isUp = true;
            event.preventDefault();
        }
    };
    // Adds event listeners to the key up and key down
    const downListener = key.downHandler.bind(key);
    const upListener = key.upHandler.bind(key);
    window.addEventListener(
        "keydown", downListener, false
    );
    window.addEventListener(
        "keyup", upListener, false
    );
    // Detach event listeners
    key.unsubscribe = () => {
        window.removeEventListener("keydown", downListener);
        window.removeEventListener("keyup", upListener);
    };
    return key
}
/**
 * If this event is activated, the global attack variable becomes true
 */
function mousedownEventHandler() {
    window.attack = true;
}
/**
 * This function sets up the controls for the user. Movement and mouse click to attack.
 */
function controls(){
    // Sets up stage to use mouse clicks
    app.stage.interactive = true;
    app.stage.hitArea = app.screen;
    app.stage.on('click', mousedownEventHandler);
    // Movement controls
    let key_left = keyboard("a"),
        key_right = keyboard("d"),//,
        key_jump = keyboard(" ");
    // Left pressed
    key_left.press = () => {
        player.vx = -5;
    };
    // Left released
    key_left.release = () => {
        if (!key_right.isDown && player.vy === 0) {
            player.vx = 0;
        }
    };
    // Right pressed
    key_right.press = () => {
        player.vx = 5;
    };
    // Right released
    key_right.release = () => {
        if (!key_left.isDown && player.vy === 0) {
            player.vx = 0;
        }
    };/*
    // Jump
    key_jump.press = () => {
        player.vy = -5;
    };
    key_jump.release = () => {
        player.vy = -0.25;
    };*/
}