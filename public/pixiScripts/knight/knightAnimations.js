/**
 * Purpose: This script file stores all the various animations for each of the knight's states. Learned to do this from
 * one of the PIXI examples on their website.
 * Source: https://pixijs.io/examples/#/sprite/animatedsprite-jet.js
 * Subject: CIS 1246 Internet Programming Part 1
 * Language: PIXI JS and Javascript
 * Edited by: Reilly Webster
 * Date: April 16, 2019
 */

/**
 * Knight's idle animation
 */
function knightIdleSprite() {
    // create an array of textures from an image path
    const idleFrames = [];
    // Loops through the various sprites
    for (let i = 0; i < 6; i++) {
        const val = i < 6 ? `0${i}` : i;

        // Spritesheet was loaded with the PIXI loader
        idleFrames.push(PIXI.Texture.from(`images/knight/idle/frame00${val}.png`));
    }
    // Returns
    return idleFrames;
}
/**
 * Knight's running sprite animation
 */
function knightRunSprite() {
    // create an array of textures from an image path
    const runFrames = [];
    // Loops through the various sprites
    for (let i = 0; i < 8; i++) {
        const val = i < 8 ? `0${i}` : i;

        // Spritesheet was loaded with the PIXI loader
        runFrames.push(PIXI.Texture.from(`images/knight/run/frame00${val}.png`));
    }
    // Returns frames
    return runFrames;
}