/**
 * Purpose: This script file setups up the player and the many states the knight uses.
 * Created by: Reilly Webster
 * Subject: CIS 1246 Internet Programming Part 1
 * Language: PIXI JS and Javascript
 * Date: April 17, 2019
 */
/**
 * Initializes the player as animated sprite, sets anchor(orgin of sprite), animation speed, starting position,
 * plays animation, sets default speed, and adds the player to the stage(canvas)
 */
let player;
function loadPlayer(){
    // Checks movement to determine sprite
    player = new PIXI.extras.AnimatedSprite(knightIdleSprite());
    player.anchor.set(0.5, 0.5);
    player.animationSpeed = 0.15;
    // Sets starting position and adds player to stage
    player.x = player.width;
    player.y = app.screen.height - (player.height);
    // Plays animation
    player.play();
    // Sets player's default speed
    player.vx = 0;
    player.vy = 0;
    // Adds player to canvas
    app.stage.addChild(player);
}
/**
 * This function checks the player's current state and changes the sprites accordingly
 */
function checkState() {
    // If player is moving, swap to run sprite and state
    if(player.vx !== 0){
        player.textures = knightRunSprite();
        // Run controls
        controls();
    }
    // If player is not moving, swap to idle sprite and state
    else if(player.vx === 0){
        player.textures = knightIdleSprite();
        // Run controls
        controls();
    }
}
/**
 * This function updates the user's position as well as corrects the user's facing direction
 */
function calcMovement(){
    // Sets default facing position
    let facing = 1;
    if(player.vx > 0){
        player.scale.x = facing;
    }
    else if(player.vx < 0){
        player.scale.x = -facing;
    }
}