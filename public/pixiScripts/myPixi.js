/**
 * Purpose: This script file sets up the game's aliases, which are functions assigned to variables. This also setups up
 * the game's canvas which initiates the player and functions.
 * Source: https://github.com/kittykatattack/learningPixi#keyboard
 * Subject: CIS 1246 Internet Programming Part 1
 * Language: PIXI JS and Javascript
 * Date: April 16, 2019
 */
/**
 * This creates all the aliases/variables for each of the utilities built into PIXI JS
 * @type {PIXI.Application}
 */
let Application = PIXI.Application,
    Container = PIXI.Container,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Sprite = PIXI.Sprite,
    Rectangle = PIXI.Rectangle;
/**
 * This creates a PIXI application and initializes its' settings.
 * @type {PIXI.Application}
 */
let app = new Application({
    width: 800,
    height: 600,
    antialias: true,
    transparent: false,
    resolution: 1
});
// Adds the stage/canvas to the body of the main document
let gameStage = document.getElementById("gameStage");
gameStage.appendChild(app.view);
// Change applications positioning
app.view.style.position = 'absolute';
app.view.style.left = '50%';
app.view.style.top = '50%';
app.view.style.transform = 'translate3d( -50%, -50%, 0 )';
// Loader adds all the necessary images and loads the setup function
loader
    .add("images/knight/idle/frame0000.png")
    .add("images/knight/run/frame0000.png")
    .add("images/knight/attack_A/frame0000.png")
    .add("images/objects/testBlock.png")
    .load(setup);
// Initializes state variable
let state;

/**
 *  Initializes the game and runs the starting code.
 */
function setup(){
    // Loads graphics
    loadGraphics();
    // Loads text
    //loadMessage();
    // Loads player
    loadPlayer();
    // Loads test block for collisions
    loadCollisionBlock();
    // Game becomes active
    state = play;
    // Adds a ticker loop to run the game at 60fps
    app.ticker.add(delta => gameLoop(delta));

}
/**
 * Uses the ticker to to run the game loop. This function is continuously running and checking the code within. In this
 * case, it is constantly checking the player's movement and current state.
 * @param delta
 */
function gameLoop(delta){
    state(delta);
    // Checks the player's current state
    checkState();
}
/**
 * This function is used to update the player's position and check for collisions
 * @param delta
 */
function play(delta){
    // Calculates the player's movement to update the player's facing direction
    calcMovement();
    // Checks for collisions
    collisionCheck();
}